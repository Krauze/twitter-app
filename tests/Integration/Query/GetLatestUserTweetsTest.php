<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Tests\Integration\Query;

use PK\TwitterWidget\Query\Application\GetLatestUserTweets;
use PK\TwitterWidget\Query\Exception\RecordNotFound;
use PK\TwitterWidget\Tests\Common\CommonTestCase;
use Symfony\Component\Messenger\MessageBusInterface;

class GetLatestUserTweetsTest extends CommonTestCase
{
    protected const USER = 'warszawa';
    protected const WRONG_USER = 'wrong_user_sfvfcnjdefne';
    protected const LIMIT = 2;

    public function test_if_get_tweets()
    {
        $tweets = $this->getLatestTweets(self::USER);
        $this->assertCount(self::LIMIT, $tweets);
    }

    public function test_it_throw_exception_when_rate_not_found()
    {
        $this->expectException(RecordNotFound::class);

        $tweet = $this->getLatestTweets(self::WRONG_USER);
        $this->assertObjectHasAttribute('errors', $tweet);
    }

    protected function getLatestTweets(string $userName)
    {
        /** @var MessageBusInterface $queryBus */
        $queryBus = static::$container->get('messenger.bus.query');

        $query = new GetLatestUserTweets(
            $userName,
            self::LIMIT
        );

        return $queryBus->dispatch($query);
    }
}
