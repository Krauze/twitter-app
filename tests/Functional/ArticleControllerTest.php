<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Tests\Functional\CustomSections;

use PK\TwitterWidget\Tests\Common\CommonTestCase;
use Symfony\Component\HttpFoundation\Response;

class ArticleControllerTest extends CommonTestCase
{
    private const ROUTE_NAME = 'article_list';
    private const USER = 'warszawa';
    private const USER_FAILED = 'warszawa_2435545';

    public function test_return_articles()
    {
        $response = $this->getHttpResponse(self::USER);
        $responseContent = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertGreaterThan(0, count($responseContent));
    }

    public function test_fails_on_wrong_user()
    {
        $response = $this->getHttpResponse(self::USER_FAILED);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    protected function getHttpResponse(string $userName): Response
    {
        $client = static::createClient();
        $url = static::$container->get('router')->generate(self::ROUTE_NAME, [
            'userName' => $userName
        ]);

        $client->request('GET', $url);

        return $client->getResponse();
    }
}
