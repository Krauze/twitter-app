<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Tests\Common;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class CommonTestCase extends WebTestCase
{
    public function setUp()
    {
        parent::setUp();
        self::bootKernel();
    }
}
