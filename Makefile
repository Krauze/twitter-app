build:
	composer install

run: build
	php -S localhost:8000 -t public/

tests: build
	./bin/phpunit
