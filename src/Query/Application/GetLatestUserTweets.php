<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Query\Application;

class GetLatestUserTweets implements Query
{
    /** @var string */
    protected $userName;

    /** @var int */
    protected $count;

    public function __construct(string $userName, int $count = 10)
    {
        $this->userName = $userName;
        $this->count = $count;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}
