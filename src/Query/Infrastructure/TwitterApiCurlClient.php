<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Query\Infrastructure;

use PK\TwitterWidget\Query\Exception\ApiRequestError;

class TwitterApiCurlClient extends CurlClient implements ApiClient
{
    protected const AUTH_URL = 'https://api.twitter.com/oauth2/token';

    protected const API_URL = 'https://api.twitter.com/1.1';

    /** @var string */
    protected $consumerKey;

    /** @var string */
    protected $consumerSecret;

    /** @var string */
    protected $bearerToken;

    public function __construct(
        string $consumerKey,
        string $consumerSecret
    ) {
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
    }

    /**
     * @inheritdoc
     */
    public function get(string $url, array $parameters = [])
    {
        $url = $this->getRequestUrl($url, $parameters);

        $response = $this->makeRequest($url, [
            CURLOPT_HTTPHEADER => [
                'Content-type: application/x-www-form-urlencoded;charset=UTF-8',
                'Authorization: Bearer ' . $this->getBearerToken()
            ]
        ]);

        return json_decode($response);
    }

    protected function getRequestUrl(string $url, array $parameters = []): string
    {
        return self::API_URL . '/' . ltrim($url, '/') . '.json?' .
            http_build_query($parameters);
    }

    protected function getBearerToken(): string
    {
        if (empty($this->bearerToken)) {
           $this->bearerToken = $this->authenticate();
        }

        return $this->bearerToken;
    }

    protected function authenticate(): string
    {
        $authKey = rawurlencode($this->consumerKey) . ':' . rawurlencode($this->consumerSecret);

        $response = $this->makeRequest(self::AUTH_URL, [
            CURLOPT_HTTPHEADER => [
                'Content-type: application/x-www-form-urlencoded;charset=UTF-8',
                'Authorization: Basic ' . base64_encode($authKey)
            ],
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => 'grant_type=client_credentials'
        ]);

        $jsonObject = json_decode($response);
        if(empty($jsonObject) || empty($jsonObject->access_token)) {
            throw new ApiRequestError('Twitter api authentication error !');
        }

        return $jsonObject->access_token;
    }
}
