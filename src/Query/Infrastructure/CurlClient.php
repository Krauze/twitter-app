<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Query\Infrastructure;

class CurlClient
{
    /**
     * @param string $url
     * @param array $options
     * @return mixed
     */
    public function makeRequest(string $url, array $options = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        foreach($options as $key => $value) {
            curl_setopt($ch, $key, $value);
        }

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}
