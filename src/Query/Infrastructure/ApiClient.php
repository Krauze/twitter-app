<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Query\Infrastructure;

interface ApiClient
{
    /**
     * @param string $url
     * @param array $parameters
     * @return mixed
     */
    public function get(string $url, array  $parameters = []);
}
