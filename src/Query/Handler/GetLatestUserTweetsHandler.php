<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Query\Handler;

use PK\TwitterWidget\Query\Application\GetLatestUserTweets;
use PK\TwitterWidget\Query\Domain\TweetView;
use PK\TwitterWidget\Query\Exception\RecordNotFound;
use PK\TwitterWidget\Query\Factory\TweetViewHydrator;
use PK\TwitterWidget\Query\Infrastructure\ApiClient;

class GetLatestUserTweetsHandler
{
    use TweetViewHydrator;

    /** @var ApiClient */
    private $client;

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param GetLatestUserTweets $query
     * @return TweetView[]
     */
    public function __invoke(GetLatestUserTweets $query)
    {
        $latestTweets = $this->client->get("statuses/user_timeline", [
            "screen_name" => $query->getUserName(),
            "count" => $query->getCount()
        ]);

        if(is_object($latestTweets)) {
            throw new RecordNotFound();
        }

        return $this->hydrate($latestTweets);
    }
}
