<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Query\Factory;

use PK\TwitterWidget\Query\Domain\TweetView;

trait TweetViewHydrator
{
    /**
     * @param mixed[] $tweets
     *
     * @return TweetView[]
     */
    protected function hydrate(array $tweets): array
    {
        $results = [];
        foreach ($tweets as $tweet) {
            $results[] = $this->hydrateOne($tweet);
        }

        return $results;
    }

    /**
     * @param $tweet
     * @return TweetView
     */
    protected function hydrateOne($tweet): TweetView
    {
        $hashTags = [];
        $tags = empty($tweet->entities->hashtags) ? [] : $tweet->entities->hashtags;

        foreach($tags as $tag) {
            $hashTags[] = $tag->text;
        }

        return new TweetView(
            $tweet->id_str,
            $tweet->text,
            (bool) $tweet->truncated,
            $tweet->user->name,
            $tweet->created_at,
            $hashTags
        );
    }

    /**
     * @param TweetView[] $tweets
     * @return mixed[]
     */
    protected function extract(array $tweets): array
    {
        $records = [];
        foreach ($tweets as $tweet) {
            $records[] = $this->extractOne($tweet);
        }

        return $records;
    }

    /**
     * @param TweetView $tweet
     * @return array
     */
    protected function extractOne(TweetView $tweet): array
    {
        $tags = array_map(function($tag) {
            return '#'.$tag;
        }, $tweet->getHashTags());

        return [
            'tweetId' => $tweet->getTweetId(),
            'header' => $tweet->getHeader(),
            'content' => $tweet->getContent(),
            'isTruncated' => $tweet->isTruncated(),
            'userName' => $tweet->getUserName(),
            'createdAt' => $tweet->getCreatedAt(),
            'hashTags' => join(', ', $tags)
        ];
    }
}
