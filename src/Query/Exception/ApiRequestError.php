<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Query\Exception;

class ApiRequestError extends \RuntimeException
{
}
