<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Query\Domain;

class TweetView
{
    /** @var string */
    protected $tweetId;

    /** @var string */
    protected $content;

    /** @var bool */
    protected $truncated;

    /** @var string */
    protected $userName;

    /** @var string */
    protected $createdAt;

    /** @var string[] */
    protected $hashTags = [];

    public function __construct(
        string $tweetId,
        string $content,
        bool $truncated,
        string $userName,
        string $createdAt,
        array $hashTags
    ) {
        $this->tweetId = $tweetId;
        $this->content = $content;
        $this->truncated = $truncated;
        $this->userName = $userName;
        $this->createdAt = $createdAt;
        $this->hashTags = $hashTags;
    }

    public function getTweetId(): string
    {
        return $this->tweetId;
    }

    public function getHeader(): string
    {
        $words = preg_split('/\s+/', $this->content);
        $words = array_slice($words, 0, 6);
        return join(' ', $words);
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function isTruncated(): bool
    {
        return $this->truncated;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @return string[]
     */
    public function getHashTags(): array
    {
        return $this->hashTags;
    }
}
