<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Console;

use PK\TwitterWidget\Query\Application\GetLatestUserTweets;
use PK\TwitterWidget\Query\Factory\TweetViewHydrator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class TwitterApiDump extends Command
{
    use TweetViewHydrator;

    protected const USER_NAME = 'user_name';
    protected const COUNT = 'count';
    protected const PATH = 'path';

    /** @var MessageBusInterface  */
    private $queryBus;

    public function __construct(
        MessageBusInterface $queryBus
    ) {
        $this->queryBus = $queryBus;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('twitter:api:test')
            ->addArgument(self::USER_NAME, InputArgument::REQUIRED, 'user nick / name')
            ->addOption(self::COUNT, 'c', InputOption::VALUE_OPTIONAL, 'number of tweets to get', 10)
            ->addOption(self::PATH, null, InputOption::VALUE_OPTIONAL, 'file path to dump data in json', '')
            ->setDescription('show n user latest tweets');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $userName = $input->getArgument(self::USER_NAME);
        $count = (int) $input->getOption(self::COUNT);
        $path = $input->getOption(self::PATH);

        $query = new GetLatestUserTweets($userName, $count);
        $latestTweets = $this->queryBus->dispatch($query);

        $tweetData = [];
        foreach ($latestTweets as $tweet) {
            $tweetData[] = $this->extractOne($tweet);
        }

        if(!empty($path)) {
            file_put_contents($path, json_encode($tweetData));
        } else {
            print_r($tweetData);
        }
    }

    protected function getFilePath(string $fileName): string
    {
        return __DIR__."/../../tests/data/$fileName";
    }
}
