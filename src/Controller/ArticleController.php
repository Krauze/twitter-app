<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Controller;

use PK\TwitterWidget\Query\Application\GetLatestUserTweets;
use PK\TwitterWidget\Query\Exception\RecordNotFound;
use PK\TwitterWidget\Query\Factory\TweetViewHydrator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class ArticleController extends AbstractController
{
    use TweetViewHydrator;

    /** @var MessageBusInterface */
    protected $queryBus;

    protected const USER_NAME = 'userName';

    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function getLatest(Request $request): JsonResponse
    {
        $userName = $request->get(self::USER_NAME);

        try {
            $query = new GetLatestUserTweets($userName);
            $tweets = $this->queryBus->dispatch($query);

            return new JsonResponse([
            "articles" => $this->extract($tweets)
          ]);
        }
        catch(RecordNotFound $exception) {
            return new JsonResponse([
                    'message' =>  $exception->getMessage()
                ],
                Response::HTTP_NOT_FOUND
            );
        }
    }
}
