<?php

declare(strict_types=1);

namespace PK\TwitterWidget\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FrontPageController extends AbstractController
{
    public function indexAction(Request $request): Response
    {
        return $this->render('frontPage.html.twig', []);
    }
}
