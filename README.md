# TwitterWidget demo app

simple app to display on page last user tweets

### Running app
simple way is to build & run project is by build in php server
with code 

```bash
    composer install
    php -S localhost:8000 -t public/
```

or if make command is present

```bash
     make run
```

### Running tests
```bash
     make tests
```
