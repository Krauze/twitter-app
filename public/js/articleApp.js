let articleApp = new Vue({
    delimiters: ['${', '}'],
    el: '#articleApp',
    data: {
        articles: [],
        userName: 'warszawa',
        errors : {
            articlesNotFound: false
        }
    },
    methods: {
        loadArticles: function () {
            this.errors.articlesNotFound = false;

            fetch('/article/latest?userName=' + this.userName)
                .then(response => {
                    if (!response.ok) {
                        throw response;
                    }
                    return response.json();
                })
                .then(response => {
                    this.articles = response.articles;
                })
                .catch( error => {
                    this.errors.articlesNotFound = true;
                })

        }
    },
    created() {
        this.loadArticles();
    }
});